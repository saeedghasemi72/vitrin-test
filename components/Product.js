import Image from "next/image";
import Link from "next/link";

const Product = ({ details }) => {
  console.log("details", details);
  return (
    <Link href={`products/${details.id}`}>
      <a className="w-1/3 p-4">
        <div className="bg-slate-50 flex h-40 p-3 rounded-md">
          <div>
            <Image
              src={details.image}
              alt={details.title}
              width={150}
              height={150}
              objectFit="contain"
            />
          </div>
          <div className="pl-3">
            <p>Name: {details.title}</p>
            <p>Category: {details.category}</p>
            <p>price: {details.price}</p>
          </div>
        </div>
      </a>
    </Link>
  );
};

export default Product;
