import Image from "next/image";
import Head from "next/head";

const ProductDetails = ({ details }) => {
  console.log("details", details);
  return (
    <section className="p-6">
      <Head>
        <title>{details.title}</title>
        <meta name="description" content={details.description} />
      </Head>
      <div>
        <div className="bg-slate-50 flex p-3 rounded-md ">
          <div>
            <Image
              src={details.image}
              alt={details.title}
              width={500}
              height={500}
              objectFit="contain"
            />
          </div>
          <div className="pl-6">
            <p>Name: {details.title}</p>
            <p>Category: {details.category}</p>
            <p>price: {details.price}</p>
            <p>
              <span>Rate: {details.rating.rate}</span>
              <span className="pl-2">Count: {details.rating.count}</span>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(
    `https://fakestoreapi.com/products/${context.query.product_id}`
  );
  const details = await res.json();
  return {
    props: {
      details,
    },
  };
}

export default ProductDetails;
