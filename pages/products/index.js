import Product from "components";

const Products = ({ products = [] }) => {
  console.log("products", products);
  return (
    <section className="p-8">
      <h1 className="text-xl text-red-500 text-center font-bold mb-6">
        Products List
      </h1>
      <div className="flex flex-wrap ">
        {products.map((prd) => (
          <Product key={prd.id} details={prd} />
        ))}
      </div>
    </section>
  );
};

export async function getServerSideProps() {
  const res = await fetch(`https://fakestoreapi.com/products`);
  const products = await res.json();
  return {
    props: {
      products,
    },
  };
}

export default Products;
