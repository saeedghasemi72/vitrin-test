import NProgress from "nprogress";
import Router from "next/router";

import "../styles/globals.css";

if (typeof window !== "undefined") {
  NProgress.configure({ easing: "ease" });

  Router.events.on("routeChangeStart", () => {
    NProgress.start();
  });

  Router.events.on("routeChangeComplete", () => {
    NProgress.done();
  });

  Router.events.on("routeChangeError", () => {
    NProgress.done();
  });
}

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
